package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_group")
public class DSGroup {
    private String dsgCode;
    private String dsgName;
    private String dsgType;
    private String dsgRemark;

    @Id
    @Column(name = "dsg_code", nullable = false, length = 36)
    public String getDsgCode() {
        return dsgCode;
    }

    public void setDsgCode(String dsgCode) {
        this.dsgCode = dsgCode;
    }

    @Basic
    @Column(name = "dsg_name", nullable = true, length = 36)
    public String getDsgName() {
        return dsgName;
    }

    public void setDsgName(String dsgName) {
        this.dsgName = dsgName;
    }

    @Basic
    @Column(name = "dsg_type", nullable = true, length = 10)
    public String getDsgType() {
        return dsgType;
    }

    public void setDsgType(String dsgType) {
        this.dsgType = dsgType;
    }

    @Basic
    @Column(name = "dsg_remark", nullable = true, length = 200)
    public String getDsgRemark() {
        return dsgRemark;
    }

    public void setDsgRemark(String dsgRemark) {
        this.dsgRemark = dsgRemark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSGroup dsGroup = (DSGroup) o;

        if (dsgCode != null ? !dsgCode.equals(dsGroup.dsgCode) : dsGroup.dsgCode != null) return false;
        if (dsgName != null ? !dsgName.equals(dsGroup.dsgName) : dsGroup.dsgName != null) return false;
        if (dsgType != null ? !dsgType.equals(dsGroup.dsgType) : dsGroup.dsgType != null) return false;
        if (dsgRemark != null ? !dsgRemark.equals(dsGroup.dsgRemark) : dsGroup.dsgRemark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dsgCode != null ? dsgCode.hashCode() : 0;
        result = 31 * result + (dsgName != null ? dsgName.hashCode() : 0);
        result = 31 * result + (dsgType != null ? dsgType.hashCode() : 0);
        result = 31 * result + (dsgRemark != null ? dsgRemark.hashCode() : 0);
        return result;
    }
}
