package com.dreammakerteam.framework.system.common;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * 异常处理
 * Created by ty850 on 2017/5/12.
 */
@ControllerAdvice
public class DmtExceptionHandler {


    @ExceptionHandler(value = DmtAjaxException.class)
    @ResponseBody
    public AjaxResult ajaxErrorHandler(HttpServletRequest req, DmtAjaxException e) throws Exception {
        AjaxResult j = new AjaxResult();
        j.setSuccess(false);
        j.setMsg(e.getMessage());
        return j;
    }


    @ExceptionHandler(value = DmtException.class)
    public ModelAndView ErrorHandler(HttpServletRequest req, DmtException e) throws Exception {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", e);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("500");
        System.out.println(e.getMessage());
        return mav;
    }


}
