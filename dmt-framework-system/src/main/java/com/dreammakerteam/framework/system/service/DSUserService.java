package com.dreammakerteam.framework.system.service;


import com.dreammakerteam.framework.system.domain.DSUser;

/**
 * 用户服务
 * Created by ty850 on 2017/5/11.
 */
public interface DSUserService {

    /**
     * 通过id获取用户
     * @param id 用户id
     * @return 用户
     */
    DSUser get(String id);

    /**
     * 通过用户名及密码获取用户（密码自动加密）
     * @param username 用户名
     * @param password 用户明文密码
     * @return 用户
     */
    DSUser getByUsernameAndPassword(String username, String password);

    /**
     * 密码加密
     * @param username 用户名
     * @param password 明文密码
     * @return 密文密码
     */
    String enCodePassword(String username, String password);


}
