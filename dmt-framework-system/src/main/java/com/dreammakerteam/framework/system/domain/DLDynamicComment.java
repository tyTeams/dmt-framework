package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_l_dynamic_comment")
public class DLDynamicComment {
    private String dldcId;
    private String dldcDynamicId;
    private String dldcUserId;
    private String dldcParentId;
    private String dldcReplyUserId;
    private String dldcContent;
    private Timestamp dldcCreateTime;
    private Byte dldcState;

    @Id
    @Column(name = "dldc_id", nullable = false, length = 36)
    public String getDldcId() {
        return dldcId;
    }

    public void setDldcId(String dldcId) {
        this.dldcId = dldcId;
    }

    @Basic
    @Column(name = "dldc_dynamic_id", nullable = true, length = 36)
    public String getDldcDynamicId() {
        return dldcDynamicId;
    }

    public void setDldcDynamicId(String dldcDynamicId) {
        this.dldcDynamicId = dldcDynamicId;
    }

    @Basic
    @Column(name = "dldc_user_id", nullable = true, length = 36)
    public String getDldcUserId() {
        return dldcUserId;
    }

    public void setDldcUserId(String dldcUserId) {
        this.dldcUserId = dldcUserId;
    }

    @Basic
    @Column(name = "dldc_parent_id", nullable = true, length = 36)
    public String getDldcParentId() {
        return dldcParentId;
    }

    public void setDldcParentId(String dldcParentId) {
        this.dldcParentId = dldcParentId;
    }

    @Basic
    @Column(name = "dldc_reply_user_id", nullable = true, length = 36)
    public String getDldcReplyUserId() {
        return dldcReplyUserId;
    }

    public void setDldcReplyUserId(String dldcReplyUserId) {
        this.dldcReplyUserId = dldcReplyUserId;
    }

    @Basic
    @Column(name = "dldc_content", nullable = true, length = 200)
    public String getDldcContent() {
        return dldcContent;
    }

    public void setDldcContent(String dldcContent) {
        this.dldcContent = dldcContent;
    }

    @Basic
    @Column(name = "dldc_create_time", nullable = true)
    public Timestamp getDldcCreateTime() {
        return dldcCreateTime;
    }

    public void setDldcCreateTime(Timestamp dldcCreateTime) {
        this.dldcCreateTime = dldcCreateTime;
    }

    @Basic
    @Column(name = "dldc_state", nullable = true)
    public Byte getDldcState() {
        return dldcState;
    }

    public void setDldcState(Byte dldcState) {
        this.dldcState = dldcState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DLDynamicComment that = (DLDynamicComment) o;

        if (dldcId != null ? !dldcId.equals(that.dldcId) : that.dldcId != null) return false;
        if (dldcDynamicId != null ? !dldcDynamicId.equals(that.dldcDynamicId) : that.dldcDynamicId != null)
            return false;
        if (dldcUserId != null ? !dldcUserId.equals(that.dldcUserId) : that.dldcUserId != null) return false;
        if (dldcParentId != null ? !dldcParentId.equals(that.dldcParentId) : that.dldcParentId != null) return false;
        if (dldcReplyUserId != null ? !dldcReplyUserId.equals(that.dldcReplyUserId) : that.dldcReplyUserId != null)
            return false;
        if (dldcContent != null ? !dldcContent.equals(that.dldcContent) : that.dldcContent != null) return false;
        if (dldcCreateTime != null ? !dldcCreateTime.equals(that.dldcCreateTime) : that.dldcCreateTime != null)
            return false;
        if (dldcState != null ? !dldcState.equals(that.dldcState) : that.dldcState != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dldcId != null ? dldcId.hashCode() : 0;
        result = 31 * result + (dldcDynamicId != null ? dldcDynamicId.hashCode() : 0);
        result = 31 * result + (dldcUserId != null ? dldcUserId.hashCode() : 0);
        result = 31 * result + (dldcParentId != null ? dldcParentId.hashCode() : 0);
        result = 31 * result + (dldcReplyUserId != null ? dldcReplyUserId.hashCode() : 0);
        result = 31 * result + (dldcContent != null ? dldcContent.hashCode() : 0);
        result = 31 * result + (dldcCreateTime != null ? dldcCreateTime.hashCode() : 0);
        result = 31 * result + (dldcState != null ? dldcState.hashCode() : 0);
        return result;
    }
}
