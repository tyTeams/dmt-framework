package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_logs")
public class DSLogs {
    private String dslId;
    private String dslType;
    private String dslType2;
    private String dslUserId;
    private String dslTargetId;
    private String dslTargetType;
    private String dslTargetId2;
    private String dslTargetType2;
    private String dslRemark;
    private Timestamp dslCreateTime;

    @Id
    @Column(name = "dsl_id", nullable = false, length = 36)
    public String getDslId() {
        return dslId;
    }

    public void setDslId(String dslId) {
        this.dslId = dslId;
    }

    @Basic
    @Column(name = "dsl_type", nullable = true, length = 10)
    public String getDslType() {
        return dslType;
    }

    public void setDslType(String dslType) {
        this.dslType = dslType;
    }

    @Basic
    @Column(name = "dsl_type2", nullable = true, length = 10)
    public String getDslType2() {
        return dslType2;
    }

    public void setDslType2(String dslType2) {
        this.dslType2 = dslType2;
    }

    @Basic
    @Column(name = "dsl_user_id", nullable = true, length = 36)
    public String getDslUserId() {
        return dslUserId;
    }

    public void setDslUserId(String dslUserId) {
        this.dslUserId = dslUserId;
    }

    @Basic
    @Column(name = "dsl_target_id", nullable = true, length = 36)
    public String getDslTargetId() {
        return dslTargetId;
    }

    public void setDslTargetId(String dslTargetId) {
        this.dslTargetId = dslTargetId;
    }

    @Basic
    @Column(name = "dsl_target_type", nullable = true, length = 10)
    public String getDslTargetType() {
        return dslTargetType;
    }

    public void setDslTargetType(String dslTargetType) {
        this.dslTargetType = dslTargetType;
    }

    @Basic
    @Column(name = "dsl_target_id2", nullable = true, length = 36)
    public String getDslTargetId2() {
        return dslTargetId2;
    }

    public void setDslTargetId2(String dslTargetId2) {
        this.dslTargetId2 = dslTargetId2;
    }

    @Basic
    @Column(name = "dsl_target_type2", nullable = true, length = 10)
    public String getDslTargetType2() {
        return dslTargetType2;
    }

    public void setDslTargetType2(String dslTargetType2) {
        this.dslTargetType2 = dslTargetType2;
    }

    @Basic
    @Column(name = "dsl_remark", nullable = true, length = 200)
    public String getDslRemark() {
        return dslRemark;
    }

    public void setDslRemark(String dslRemark) {
        this.dslRemark = dslRemark;
    }

    @Basic
    @Column(name = "dsl_create_time", nullable = true)
    public Timestamp getDslCreateTime() {
        return dslCreateTime;
    }

    public void setDslCreateTime(Timestamp dslCreateTime) {
        this.dslCreateTime = dslCreateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSLogs dsLogs = (DSLogs) o;

        if (dslId != null ? !dslId.equals(dsLogs.dslId) : dsLogs.dslId != null) return false;
        if (dslType != null ? !dslType.equals(dsLogs.dslType) : dsLogs.dslType != null) return false;
        if (dslType2 != null ? !dslType2.equals(dsLogs.dslType2) : dsLogs.dslType2 != null) return false;
        if (dslUserId != null ? !dslUserId.equals(dsLogs.dslUserId) : dsLogs.dslUserId != null) return false;
        if (dslTargetId != null ? !dslTargetId.equals(dsLogs.dslTargetId) : dsLogs.dslTargetId != null) return false;
        if (dslTargetType != null ? !dslTargetType.equals(dsLogs.dslTargetType) : dsLogs.dslTargetType != null)
            return false;
        if (dslTargetId2 != null ? !dslTargetId2.equals(dsLogs.dslTargetId2) : dsLogs.dslTargetId2 != null)
            return false;
        if (dslTargetType2 != null ? !dslTargetType2.equals(dsLogs.dslTargetType2) : dsLogs.dslTargetType2 != null)
            return false;
        if (dslRemark != null ? !dslRemark.equals(dsLogs.dslRemark) : dsLogs.dslRemark != null) return false;
        if (dslCreateTime != null ? !dslCreateTime.equals(dsLogs.dslCreateTime) : dsLogs.dslCreateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dslId != null ? dslId.hashCode() : 0;
        result = 31 * result + (dslType != null ? dslType.hashCode() : 0);
        result = 31 * result + (dslType2 != null ? dslType2.hashCode() : 0);
        result = 31 * result + (dslUserId != null ? dslUserId.hashCode() : 0);
        result = 31 * result + (dslTargetId != null ? dslTargetId.hashCode() : 0);
        result = 31 * result + (dslTargetType != null ? dslTargetType.hashCode() : 0);
        result = 31 * result + (dslTargetId2 != null ? dslTargetId2.hashCode() : 0);
        result = 31 * result + (dslTargetType2 != null ? dslTargetType2.hashCode() : 0);
        result = 31 * result + (dslRemark != null ? dslRemark.hashCode() : 0);
        result = 31 * result + (dslCreateTime != null ? dslCreateTime.hashCode() : 0);
        return result;
    }
}
