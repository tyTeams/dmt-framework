package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_r_user_group")
public class DSRUserGroup {
    private String dsrugId;
    private String dsrugUserId;
    private String dsrugCode;

    @Id
    @Column(name = "dsrug_id", nullable = false, length = 36)
    public String getDsrugId() {
        return dsrugId;
    }

    public void setDsrugId(String dsrugId) {
        this.dsrugId = dsrugId;
    }

    @Basic
    @Column(name = "dsrug_user_id", nullable = true, length = 36)
    public String getDsrugUserId() {
        return dsrugUserId;
    }

    public void setDsrugUserId(String dsrugUserId) {
        this.dsrugUserId = dsrugUserId;
    }

    @Basic
    @Column(name = "dsrug_code", nullable = true, length = 36)
    public String getDsrugCode() {
        return dsrugCode;
    }

    public void setDsrugCode(String dsrugCode) {
        this.dsrugCode = dsrugCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSRUserGroup that = (DSRUserGroup) o;

        if (dsrugId != null ? !dsrugId.equals(that.dsrugId) : that.dsrugId != null) return false;
        if (dsrugUserId != null ? !dsrugUserId.equals(that.dsrugUserId) : that.dsrugUserId != null) return false;
        if (dsrugCode != null ? !dsrugCode.equals(that.dsrugCode) : that.dsrugCode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dsrugId != null ? dsrugId.hashCode() : 0;
        result = 31 * result + (dsrugUserId != null ? dsrugUserId.hashCode() : 0);
        result = 31 * result + (dsrugCode != null ? dsrugCode.hashCode() : 0);
        return result;
    }
}
