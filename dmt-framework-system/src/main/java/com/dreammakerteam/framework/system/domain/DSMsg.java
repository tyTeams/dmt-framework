package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_msg")
public class DSMsg {
    private String dsmId;
    private String dsmTargetId;
    private String dsmType;
    private String dsmTitle;
    private String dsmContent;
    private Byte dsmState;
    private Timestamp dsmCreatTime;
    private Timestamp dsmSeeTime;

    @Id
    @Column(name = "dsm_id", nullable = false, length = 36)
    public String getDsmId() {
        return dsmId;
    }

    public void setDsmId(String dsmId) {
        this.dsmId = dsmId;
    }

    @Basic
    @Column(name = "dsm_target_id", nullable = true, length = 36)
    public String getDsmTargetId() {
        return dsmTargetId;
    }

    public void setDsmTargetId(String dsmTargetId) {
        this.dsmTargetId = dsmTargetId;
    }

    @Basic
    @Column(name = "dsm_type", nullable = true, length = 10)
    public String getDsmType() {
        return dsmType;
    }

    public void setDsmType(String dsmType) {
        this.dsmType = dsmType;
    }

    @Basic
    @Column(name = "dsm_title", nullable = true, length = 50)
    public String getDsmTitle() {
        return dsmTitle;
    }

    public void setDsmTitle(String dsmTitle) {
        this.dsmTitle = dsmTitle;
    }

    @Basic
    @Column(name = "dsm_content", nullable = true, length = 200)
    public String getDsmContent() {
        return dsmContent;
    }

    public void setDsmContent(String dsmContent) {
        this.dsmContent = dsmContent;
    }

    @Basic
    @Column(name = "dsm_state", nullable = true)
    public Byte getDsmState() {
        return dsmState;
    }

    public void setDsmState(Byte dsmState) {
        this.dsmState = dsmState;
    }

    @Basic
    @Column(name = "dsm_creat_time", nullable = true)
    public Timestamp getDsmCreatTime() {
        return dsmCreatTime;
    }

    public void setDsmCreatTime(Timestamp dsmCreatTime) {
        this.dsmCreatTime = dsmCreatTime;
    }

    @Basic
    @Column(name = "dsm_see_time", nullable = true)
    public Timestamp getDsmSeeTime() {
        return dsmSeeTime;
    }

    public void setDsmSeeTime(Timestamp dsmSeeTime) {
        this.dsmSeeTime = dsmSeeTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSMsg dsMsg = (DSMsg) o;

        if (dsmId != null ? !dsmId.equals(dsMsg.dsmId) : dsMsg.dsmId != null) return false;
        if (dsmTargetId != null ? !dsmTargetId.equals(dsMsg.dsmTargetId) : dsMsg.dsmTargetId != null) return false;
        if (dsmType != null ? !dsmType.equals(dsMsg.dsmType) : dsMsg.dsmType != null) return false;
        if (dsmTitle != null ? !dsmTitle.equals(dsMsg.dsmTitle) : dsMsg.dsmTitle != null) return false;
        if (dsmContent != null ? !dsmContent.equals(dsMsg.dsmContent) : dsMsg.dsmContent != null) return false;
        if (dsmState != null ? !dsmState.equals(dsMsg.dsmState) : dsMsg.dsmState != null) return false;
        if (dsmCreatTime != null ? !dsmCreatTime.equals(dsMsg.dsmCreatTime) : dsMsg.dsmCreatTime != null) return false;
        if (dsmSeeTime != null ? !dsmSeeTime.equals(dsMsg.dsmSeeTime) : dsMsg.dsmSeeTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dsmId != null ? dsmId.hashCode() : 0;
        result = 31 * result + (dsmTargetId != null ? dsmTargetId.hashCode() : 0);
        result = 31 * result + (dsmType != null ? dsmType.hashCode() : 0);
        result = 31 * result + (dsmTitle != null ? dsmTitle.hashCode() : 0);
        result = 31 * result + (dsmContent != null ? dsmContent.hashCode() : 0);
        result = 31 * result + (dsmState != null ? dsmState.hashCode() : 0);
        result = 31 * result + (dsmCreatTime != null ? dsmCreatTime.hashCode() : 0);
        result = 31 * result + (dsmSeeTime != null ? dsmSeeTime.hashCode() : 0);
        return result;
    }
}
