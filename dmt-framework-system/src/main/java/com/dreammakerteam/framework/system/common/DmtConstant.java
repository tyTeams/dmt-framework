package com.dreammakerteam.framework.system.common;

/**
 * DMT-常量集
 * Created by ty850 on 2017/5/12.
 */
public class DmtConstant {
    /** 成功 */
    public final static String SUCCESS = "成功";
    /** 未知异常 */
    public final static String UNKNOWN_EXCEPTION = "未知异常";

    /** 状态-删除 */
    public final static Byte STATE_DELETE = -1;
    /** 状态-正常 */
    public final static Byte STATE_NORMAL = 0;
    /** 状态-隐藏 */
    public final static Byte STATE_HIDE = -2;

}
