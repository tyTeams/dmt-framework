package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_authority")
public class DSAuthority {
    private String dsaCode;
    private String dsaName;
    private String dsaRemark;

    @Id
    @Column(name = "dsa_code", nullable = false, length = 36)
    public String getDsaCode() {
        return dsaCode;
    }

    public void setDsaCode(String dsaCode) {
        this.dsaCode = dsaCode;
    }

    @Basic
    @Column(name = "dsa_name", nullable = true, length = 36)
    public String getDsaName() {
        return dsaName;
    }

    public void setDsaName(String dsaName) {
        this.dsaName = dsaName;
    }

    @Basic
    @Column(name = "dsa_remark", nullable = true, length = 50)
    public String getDsaRemark() {
        return dsaRemark;
    }

    public void setDsaRemark(String dsaRemark) {
        this.dsaRemark = dsaRemark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSAuthority that = (DSAuthority) o;

        if (dsaCode != null ? !dsaCode.equals(that.dsaCode) : that.dsaCode != null) return false;
        if (dsaName != null ? !dsaName.equals(that.dsaName) : that.dsaName != null) return false;
        if (dsaRemark != null ? !dsaRemark.equals(that.dsaRemark) : that.dsaRemark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dsaCode != null ? dsaCode.hashCode() : 0;
        result = 31 * result + (dsaName != null ? dsaName.hashCode() : 0);
        result = 31 * result + (dsaRemark != null ? dsaRemark.hashCode() : 0);
        return result;
    }
}
