package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_params")
public class DSParams {
    private String dspKey;
    private String dspValue;
    private String dspParent;

    @Id
    @Column(name = "dsp_key", nullable = false, length = 36)
    public String getDspKey() {
        return dspKey;
    }

    public void setDspKey(String dspKey) {
        this.dspKey = dspKey;
    }

    @Basic
    @Column(name = "dsp_value", nullable = true, length = 36)
    public String getDspValue() {
        return dspValue;
    }

    public void setDspValue(String dspValue) {
        this.dspValue = dspValue;
    }

    @Basic
    @Column(name = "dsp_parent", nullable = true, length = 36)
    public String getDspParent() {
        return dspParent;
    }

    public void setDspParent(String dspParent) {
        this.dspParent = dspParent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSParams dsParams = (DSParams) o;

        if (dspKey != null ? !dspKey.equals(dsParams.dspKey) : dsParams.dspKey != null) return false;
        if (dspValue != null ? !dspValue.equals(dsParams.dspValue) : dsParams.dspValue != null) return false;
        if (dspParent != null ? !dspParent.equals(dsParams.dspParent) : dsParams.dspParent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dspKey != null ? dspKey.hashCode() : 0;
        result = 31 * result + (dspValue != null ? dspValue.hashCode() : 0);
        result = 31 * result + (dspParent != null ? dspParent.hashCode() : 0);
        return result;
    }
}
