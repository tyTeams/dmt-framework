package com.dreammakerteam.framework.system.common;

/**
 * Ajax请求异常，该异常会返回json信息
 * Created by ty850 on 2017/5/12.
 */
public class DmtAjaxException extends DmtException {
    public DmtAjaxException(String message) {
        super(message);
    }
}
