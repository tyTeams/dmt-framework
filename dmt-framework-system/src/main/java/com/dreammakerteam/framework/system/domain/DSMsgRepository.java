package com.dreammakerteam.framework.system.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ty850 on 2017/5/11.
 */
public interface DSMsgRepository extends JpaRepository<DSMsg, String> {
}
