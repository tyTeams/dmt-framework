package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_l_pandect")
public class DLPandect {
    private String dlpId;
    private Timestamp dlpCreateTime;
    private Timestamp dlpEndTime;
    private Byte dlpState;
    private String dlpLestId;

    @Id
    @Column(name = "dlp_id", nullable = false, length = 36)
    public String getDlpId() {
        return dlpId;
    }

    public void setDlpId(String dlpId) {
        this.dlpId = dlpId;
    }

    @Basic
    @Column(name = "dlp_create_time", nullable = true)
    public Timestamp getDlpCreateTime() {
        return dlpCreateTime;
    }

    public void setDlpCreateTime(Timestamp dlpCreateTime) {
        this.dlpCreateTime = dlpCreateTime;
    }

    @Basic
    @Column(name = "dlp_end_time", nullable = true)
    public Timestamp getDlpEndTime() {
        return dlpEndTime;
    }

    public void setDlpEndTime(Timestamp dlpEndTime) {
        this.dlpEndTime = dlpEndTime;
    }

    @Basic
    @Column(name = "dlp_state", nullable = true)
    public Byte getDlpState() {
        return dlpState;
    }

    public void setDlpState(Byte dlpState) {
        this.dlpState = dlpState;
    }

    @Basic
    @Column(name = "dlp_lest_id", nullable = true, length = 36)
    public String getDlpLestId() {
        return dlpLestId;
    }

    public void setDlpLestId(String dlpLestId) {
        this.dlpLestId = dlpLestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DLPandect dlPandect = (DLPandect) o;

        if (dlpId != null ? !dlpId.equals(dlPandect.dlpId) : dlPandect.dlpId != null) return false;
        if (dlpCreateTime != null ? !dlpCreateTime.equals(dlPandect.dlpCreateTime) : dlPandect.dlpCreateTime != null)
            return false;
        if (dlpEndTime != null ? !dlpEndTime.equals(dlPandect.dlpEndTime) : dlPandect.dlpEndTime != null) return false;
        if (dlpState != null ? !dlpState.equals(dlPandect.dlpState) : dlPandect.dlpState != null) return false;
        if (dlpLestId != null ? !dlpLestId.equals(dlPandect.dlpLestId) : dlPandect.dlpLestId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dlpId != null ? dlpId.hashCode() : 0;
        result = 31 * result + (dlpCreateTime != null ? dlpCreateTime.hashCode() : 0);
        result = 31 * result + (dlpEndTime != null ? dlpEndTime.hashCode() : 0);
        result = 31 * result + (dlpState != null ? dlpState.hashCode() : 0);
        result = 31 * result + (dlpLestId != null ? dlpLestId.hashCode() : 0);
        return result;
    }
}
