package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_user")
public class DSUser {
    private String dsuId;
    private String dsuUsername;
    private String dsuName;
    private String dsuPassword;
    private Timestamp dsuLastLoginTime;
    private String dsuGroupCode;
    private Timestamp dsuChangeTime;
    private String dsuPhoto;
    private String dsuRemark;
    private Timestamp dsuCreateTime;
    private Byte dsuState;

    @Id
    @Column(name = "dsu_id", nullable = false, length = 36)
    public String getDsuId() {
        return dsuId;
    }

    public void setDsuId(String dsuId) {
        this.dsuId = dsuId;
    }

    @Basic
    @Column(name = "dsu_username", nullable = true, length = 36)
    public String getDsuUsername() {
        return dsuUsername;
    }

    public void setDsuUsername(String dsuUsername) {
        this.dsuUsername = dsuUsername;
    }

    @Basic
    @Column(name = "dsu_name", nullable = true, length = 36)
    public String getDsuName() {
        return dsuName;
    }

    public void setDsuName(String dsuName) {
        this.dsuName = dsuName;
    }

    @Basic
    @Column(name = "dsu_password", nullable = true, length = 36)
    public String getDsuPassword() {
        return dsuPassword;
    }

    public void setDsuPassword(String dsuPassword) {
        this.dsuPassword = dsuPassword;
    }

    @Basic
    @Column(name = "dsu_last_login_time", nullable = true)
    public Timestamp getDsuLastLoginTime() {
        return dsuLastLoginTime;
    }

    public void setDsuLastLoginTime(Timestamp dsuLastLoginTime) {
        this.dsuLastLoginTime = dsuLastLoginTime;
    }

    @Basic
    @Column(name = "dsu_group_code", nullable = true, length = 36)
    public String getDsuGroupCode() {
        return dsuGroupCode;
    }

    public void setDsuGroupCode(String dsuGroupCode) {
        this.dsuGroupCode = dsuGroupCode;
    }

    @Basic
    @Column(name = "dsu_change_time", nullable = true)
    public Timestamp getDsuChangeTime() {
        return dsuChangeTime;
    }

    public void setDsuChangeTime(Timestamp dsuChangeTime) {
        this.dsuChangeTime = dsuChangeTime;
    }

    @Basic
    @Column(name = "dsu_photo", nullable = true, length = 50)
    public String getDsuPhoto() {
        return dsuPhoto;
    }

    public void setDsuPhoto(String dsuPhoto) {
        this.dsuPhoto = dsuPhoto;
    }

    @Basic
    @Column(name = "dsu_remark", nullable = true, length = 200)
    public String getDsuRemark() {
        return dsuRemark;
    }

    public void setDsuRemark(String dsuRemark) {
        this.dsuRemark = dsuRemark;
    }

    @Basic
    @Column(name = "dsu_create_time", nullable = true)
    public Timestamp getDsuCreateTime() {
        return dsuCreateTime;
    }

    public void setDsuCreateTime(Timestamp dsuCreateTime) {
        this.dsuCreateTime = dsuCreateTime;
    }

    @Basic
    @Column(name = "dsu_state", nullable = true)
    public Byte getDsuState() {
        return dsuState;
    }

    public void setDsuState(Byte dsuState) {
        this.dsuState = dsuState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSUser dsUser = (DSUser) o;

        if (dsuId != null ? !dsuId.equals(dsUser.dsuId) : dsUser.dsuId != null) return false;
        if (dsuUsername != null ? !dsuUsername.equals(dsUser.dsuUsername) : dsUser.dsuUsername != null) return false;
        if (dsuName != null ? !dsuName.equals(dsUser.dsuName) : dsUser.dsuName != null) return false;
        if (dsuPassword != null ? !dsuPassword.equals(dsUser.dsuPassword) : dsUser.dsuPassword != null) return false;
        if (dsuLastLoginTime != null ? !dsuLastLoginTime.equals(dsUser.dsuLastLoginTime) : dsUser.dsuLastLoginTime != null)
            return false;
        if (dsuGroupCode != null ? !dsuGroupCode.equals(dsUser.dsuGroupCode) : dsUser.dsuGroupCode != null)
            return false;
        if (dsuChangeTime != null ? !dsuChangeTime.equals(dsUser.dsuChangeTime) : dsUser.dsuChangeTime != null)
            return false;
        if (dsuPhoto != null ? !dsuPhoto.equals(dsUser.dsuPhoto) : dsUser.dsuPhoto != null) return false;
        if (dsuRemark != null ? !dsuRemark.equals(dsUser.dsuRemark) : dsUser.dsuRemark != null) return false;
        if (dsuCreateTime != null ? !dsuCreateTime.equals(dsUser.dsuCreateTime) : dsUser.dsuCreateTime != null)
            return false;
        if (dsuState != null ? !dsuState.equals(dsUser.dsuState) : dsUser.dsuState != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dsuId != null ? dsuId.hashCode() : 0;
        result = 31 * result + (dsuUsername != null ? dsuUsername.hashCode() : 0);
        result = 31 * result + (dsuName != null ? dsuName.hashCode() : 0);
        result = 31 * result + (dsuPassword != null ? dsuPassword.hashCode() : 0);
        result = 31 * result + (dsuLastLoginTime != null ? dsuLastLoginTime.hashCode() : 0);
        result = 31 * result + (dsuGroupCode != null ? dsuGroupCode.hashCode() : 0);
        result = 31 * result + (dsuChangeTime != null ? dsuChangeTime.hashCode() : 0);
        result = 31 * result + (dsuPhoto != null ? dsuPhoto.hashCode() : 0);
        result = 31 * result + (dsuRemark != null ? dsuRemark.hashCode() : 0);
        result = 31 * result + (dsuCreateTime != null ? dsuCreateTime.hashCode() : 0);
        result = 31 * result + (dsuState != null ? dsuState.hashCode() : 0);
        return result;
    }
}
