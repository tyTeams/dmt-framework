package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_l_dynamic_attachment")
public class DLDynamicAttachment {
    private String dldaId;
    private String dldaDynamicId;
    private String dldaAttachmentId;
    private Byte dldaState;

    @Id
    @Column(name = "dlda_id", nullable = false, length = 36)
    public String getDldaId() {
        return dldaId;
    }

    public void setDldaId(String dldaId) {
        this.dldaId = dldaId;
    }

    @Basic
    @Column(name = "dlda_dynamic_id", nullable = true, length = 36)
    public String getDldaDynamicId() {
        return dldaDynamicId;
    }

    public void setDldaDynamicId(String dldaDynamicId) {
        this.dldaDynamicId = dldaDynamicId;
    }

    @Basic
    @Column(name = "dlda_attachment_id", nullable = true, length = 36)
    public String getDldaAttachmentId() {
        return dldaAttachmentId;
    }

    public void setDldaAttachmentId(String dldaAttachmentId) {
        this.dldaAttachmentId = dldaAttachmentId;
    }

    @Basic
    @Column(name = "dlda_state", nullable = true)
    public Byte getDldaState() {
        return dldaState;
    }

    public void setDldaState(Byte dldaState) {
        this.dldaState = dldaState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DLDynamicAttachment that = (DLDynamicAttachment) o;

        if (dldaId != null ? !dldaId.equals(that.dldaId) : that.dldaId != null) return false;
        if (dldaDynamicId != null ? !dldaDynamicId.equals(that.dldaDynamicId) : that.dldaDynamicId != null)
            return false;
        if (dldaAttachmentId != null ? !dldaAttachmentId.equals(that.dldaAttachmentId) : that.dldaAttachmentId != null)
            return false;
        if (dldaState != null ? !dldaState.equals(that.dldaState) : that.dldaState != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dldaId != null ? dldaId.hashCode() : 0;
        result = 31 * result + (dldaDynamicId != null ? dldaDynamicId.hashCode() : 0);
        result = 31 * result + (dldaAttachmentId != null ? dldaAttachmentId.hashCode() : 0);
        result = 31 * result + (dldaState != null ? dldaState.hashCode() : 0);
        return result;
    }
}
