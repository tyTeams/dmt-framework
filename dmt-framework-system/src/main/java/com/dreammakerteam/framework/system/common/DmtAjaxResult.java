package com.dreammakerteam.framework.system.common;

/**
 * DMT-Ajax请求的返回值
 * Created by ty850 on 2017/5/12.
 */
public class DmtAjaxResult {
    private boolean success = true;
    private String msg = Constant.SUCCESS;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
