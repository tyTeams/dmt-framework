package com.dreammakerteam.framework.system.common;

/**
 * 异常
 * Created by ty850 on 2017/5/12.
 */
public class DmtException extends RuntimeException {
    public DmtException(String message) {
        super(message);
    }
}
