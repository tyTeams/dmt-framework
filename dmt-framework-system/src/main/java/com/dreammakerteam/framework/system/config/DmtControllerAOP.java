package com.dreammakerteam.framework.system.config;

import com.dreammakerteam.framework.system.common.DmtAjaxException;
import com.dreammakerteam.framework.system.common.DmtConstant;
import com.dreammakerteam.framework.system.common.DmtException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 捕捉异常的AOP, 捕捉controller包下结尾为Controller的class的所有有RequestMapping注解的方法
 * + 日志
 * Created by ty850 on 2017/5/12.
 */
@Aspect
public class DmtControllerAOP {

    @Pointcut("execution(@org.springframework.web.bind.annotation.RequestMapping public * *..controller..*Controller.*(..))")
    public void exception(){}

    @Around("exception()")
    public void around(ProceedingJoinPoint pjp) throws Throwable {
        try {
            pjp.proceed();
        } catch (Exception e) {
            Method method = ((MethodSignature) pjp.getSignature()).getMethod();

            if (method.isAnnotationPresent(ResponseBody.class) || method.getDeclaringClass().isAnnotationPresent(RestController.class)) {
                // 如果有ResponseBody获取类上有RestController注解
                if (e instanceof DmtAjaxException)
                    throw e;
                else if (e instanceof DmtException)
                    throw new DmtAjaxException(e.getMessage());
                else
                    throw new DmtAjaxException(DmtConstant.UNKNOWN_EXCEPTION);
            } else {
                // 如果没有ResponseBody
                if (e instanceof DmtAjaxException)
                    throw new DmtException(e.getMessage());
                else if (e instanceof DmtException)
                    throw e;
                else
                    throw new DmtException(DmtConstant.UNKNOWN_EXCEPTION);
            }
        }
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        System.out.println(request.getRequestURL().toString());
    }
}
