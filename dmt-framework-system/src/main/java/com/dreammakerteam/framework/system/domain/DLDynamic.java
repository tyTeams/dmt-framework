package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_l_dynamic")
public class DLDynamic {
    private String dldId;
    private String dldPandectId;
    private String dldUserId;
    private String dldContent;
    private Timestamp dldCreateTime;
    private Byte dldState;
    private Byte dldLoveValue;
    private Byte dldType;

    @Id
    @Column(name = "dld_id", nullable = false, length = 36)
    public String getDldId() {
        return dldId;
    }

    public void setDldId(String dldId) {
        this.dldId = dldId;
    }

    @Basic
    @Column(name = "dld_pandect_id", nullable = true, length = 36)
    public String getDldPandectId() {
        return dldPandectId;
    }

    public void setDldPandectId(String dldPandectId) {
        this.dldPandectId = dldPandectId;
    }

    @Basic
    @Column(name = "dld_user_id", nullable = true, length = 36)
    public String getDldUserId() {
        return dldUserId;
    }

    public void setDldUserId(String dldUserId) {
        this.dldUserId = dldUserId;
    }

    @Basic
    @Column(name = "dld_content", nullable = true, length = 1000)
    public String getDldContent() {
        return dldContent;
    }

    public void setDldContent(String dldContent) {
        this.dldContent = dldContent;
    }

    @Basic
    @Column(name = "dld_create_time", nullable = true)
    public Timestamp getDldCreateTime() {
        return dldCreateTime;
    }

    public void setDldCreateTime(Timestamp dldCreateTime) {
        this.dldCreateTime = dldCreateTime;
    }

    @Basic
    @Column(name = "dld_state", nullable = true)
    public Byte getDldState() {
        return dldState;
    }

    public void setDldState(Byte dldState) {
        this.dldState = dldState;
    }

    @Basic
    @Column(name = "dld_love_value", nullable = true)
    public Byte getDldLoveValue() {
        return dldLoveValue;
    }

    public void setDldLoveValue(Byte dldLoveValue) {
        this.dldLoveValue = dldLoveValue;
    }

    @Basic
    @Column(name = "dld_type", nullable = true)
    public Byte getDldType() {
        return dldType;
    }

    public void setDldType(Byte dldType) {
        this.dldType = dldType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DLDynamic dlDynamic = (DLDynamic) o;

        if (dldId != null ? !dldId.equals(dlDynamic.dldId) : dlDynamic.dldId != null) return false;
        if (dldPandectId != null ? !dldPandectId.equals(dlDynamic.dldPandectId) : dlDynamic.dldPandectId != null)
            return false;
        if (dldUserId != null ? !dldUserId.equals(dlDynamic.dldUserId) : dlDynamic.dldUserId != null) return false;
        if (dldContent != null ? !dldContent.equals(dlDynamic.dldContent) : dlDynamic.dldContent != null) return false;
        if (dldCreateTime != null ? !dldCreateTime.equals(dlDynamic.dldCreateTime) : dlDynamic.dldCreateTime != null)
            return false;
        if (dldState != null ? !dldState.equals(dlDynamic.dldState) : dlDynamic.dldState != null) return false;
        if (dldLoveValue != null ? !dldLoveValue.equals(dlDynamic.dldLoveValue) : dlDynamic.dldLoveValue != null)
            return false;
        if (dldType != null ? !dldType.equals(dlDynamic.dldType) : dlDynamic.dldType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dldId != null ? dldId.hashCode() : 0;
        result = 31 * result + (dldPandectId != null ? dldPandectId.hashCode() : 0);
        result = 31 * result + (dldUserId != null ? dldUserId.hashCode() : 0);
        result = 31 * result + (dldContent != null ? dldContent.hashCode() : 0);
        result = 31 * result + (dldCreateTime != null ? dldCreateTime.hashCode() : 0);
        result = 31 * result + (dldState != null ? dldState.hashCode() : 0);
        result = 31 * result + (dldLoveValue != null ? dldLoveValue.hashCode() : 0);
        result = 31 * result + (dldType != null ? dldType.hashCode() : 0);
        return result;
    }
}
