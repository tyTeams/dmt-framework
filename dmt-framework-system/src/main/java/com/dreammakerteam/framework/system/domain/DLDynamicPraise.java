package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_l_dynamic_praise")
public class DLDynamicPraise {
    private String dldpId;
    private String dldpDynamicId;
    private String dldpUserId;
    private Timestamp dldpCreateTime;

    @Id
    @Column(name = "dldp_id", nullable = false, length = 36)
    public String getDldpId() {
        return dldpId;
    }

    public void setDldpId(String dldpId) {
        this.dldpId = dldpId;
    }

    @Basic
    @Column(name = "dldp_dynamic_id", nullable = true, length = 36)
    public String getDldpDynamicId() {
        return dldpDynamicId;
    }

    public void setDldpDynamicId(String dldpDynamicId) {
        this.dldpDynamicId = dldpDynamicId;
    }

    @Basic
    @Column(name = "dldp_user_id", nullable = true, length = 36)
    public String getDldpUserId() {
        return dldpUserId;
    }

    public void setDldpUserId(String dldpUserId) {
        this.dldpUserId = dldpUserId;
    }

    @Basic
    @Column(name = "dldp_create_time", nullable = true)
    public Timestamp getDldpCreateTime() {
        return dldpCreateTime;
    }

    public void setDldpCreateTime(Timestamp dldpCreateTime) {
        this.dldpCreateTime = dldpCreateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DLDynamicPraise that = (DLDynamicPraise) o;

        if (dldpId != null ? !dldpId.equals(that.dldpId) : that.dldpId != null) return false;
        if (dldpDynamicId != null ? !dldpDynamicId.equals(that.dldpDynamicId) : that.dldpDynamicId != null)
            return false;
        if (dldpUserId != null ? !dldpUserId.equals(that.dldpUserId) : that.dldpUserId != null) return false;
        if (dldpCreateTime != null ? !dldpCreateTime.equals(that.dldpCreateTime) : that.dldpCreateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dldpId != null ? dldpId.hashCode() : 0;
        result = 31 * result + (dldpDynamicId != null ? dldpDynamicId.hashCode() : 0);
        result = 31 * result + (dldpUserId != null ? dldpUserId.hashCode() : 0);
        result = 31 * result + (dldpCreateTime != null ? dldpCreateTime.hashCode() : 0);
        return result;
    }
}
