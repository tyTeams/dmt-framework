package com.dreammakerteam.framework.system.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * Created by ty850 on 2017/5/11.
 */
public interface DSUserRepository extends JpaRepository<DSUser, String> {


    /**
     * 根据用户名以及密码获取用户对象
     * @param username 用户名
     * @param password 密码
     * @return 用户实体
     */
    DSUser findByDsuUsernameAndDsuPassword(String username, String password);
}
