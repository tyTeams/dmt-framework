package com.dreammakerteam.framework.system.common;

import java.util.HashMap;

/**
 * Dmt-封装HashMap
 * 1.连续赋值
 * Created by ty850 on 2017/5/13.
 */
public class DmtHashMap<K, V> extends HashMap<K, V> {
    public DmtHashMap<K, V> add(K key, V value) {
        this.put(key, value);
        return this;
    }
}
