package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_r_group_authority")
public class DSRGroupAuthority {
    private String dsrgaId;
    private String dsrgaGroupCode;
    private String dsrgaAuthorityCode;
    private String dsrgaRemark;

    @Id
    @Column(name = "dsrga_id", nullable = false, length = 36)
    public String getDsrgaId() {
        return dsrgaId;
    }

    public void setDsrgaId(String dsrgaId) {
        this.dsrgaId = dsrgaId;
    }

    @Basic
    @Column(name = "dsrga_group_code", nullable = true, length = 36)
    public String getDsrgaGroupCode() {
        return dsrgaGroupCode;
    }

    public void setDsrgaGroupCode(String dsrgaGroupCode) {
        this.dsrgaGroupCode = dsrgaGroupCode;
    }

    @Basic
    @Column(name = "dsrga_authority_code", nullable = true, length = 36)
    public String getDsrgaAuthorityCode() {
        return dsrgaAuthorityCode;
    }

    public void setDsrgaAuthorityCode(String dsrgaAuthorityCode) {
        this.dsrgaAuthorityCode = dsrgaAuthorityCode;
    }

    @Basic
    @Column(name = "dsrga_remark", nullable = true, length = 100)
    public String getDsrgaRemark() {
        return dsrgaRemark;
    }

    public void setDsrgaRemark(String dsrgaRemark) {
        this.dsrgaRemark = dsrgaRemark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSRGroupAuthority that = (DSRGroupAuthority) o;

        if (dsrgaId != null ? !dsrgaId.equals(that.dsrgaId) : that.dsrgaId != null) return false;
        if (dsrgaGroupCode != null ? !dsrgaGroupCode.equals(that.dsrgaGroupCode) : that.dsrgaGroupCode != null)
            return false;
        if (dsrgaAuthorityCode != null ? !dsrgaAuthorityCode.equals(that.dsrgaAuthorityCode) : that.dsrgaAuthorityCode != null)
            return false;
        if (dsrgaRemark != null ? !dsrgaRemark.equals(that.dsrgaRemark) : that.dsrgaRemark != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dsrgaId != null ? dsrgaId.hashCode() : 0;
        result = 31 * result + (dsrgaGroupCode != null ? dsrgaGroupCode.hashCode() : 0);
        result = 31 * result + (dsrgaAuthorityCode != null ? dsrgaAuthorityCode.hashCode() : 0);
        result = 31 * result + (dsrgaRemark != null ? dsrgaRemark.hashCode() : 0);
        return result;
    }
}
