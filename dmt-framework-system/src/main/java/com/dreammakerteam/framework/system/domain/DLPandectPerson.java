package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_l_pandect_person")
public class DLPandectPerson {
    private String dlppId;
    private String dlppPandectId;
    private Byte dlppState;
    private Integer dlppLoveValue;

    @Id
    @Column(name = "dlpp_id", nullable = false, length = 36)
    public String getDlppId() {
        return dlppId;
    }

    public void setDlppId(String dlppId) {
        this.dlppId = dlppId;
    }

    @Basic
    @Column(name = "dlpp_pandect_id", nullable = true, length = 36)
    public String getDlppPandectId() {
        return dlppPandectId;
    }

    public void setDlppPandectId(String dlppPandectId) {
        this.dlppPandectId = dlppPandectId;
    }

    @Basic
    @Column(name = "dlpp_state", nullable = true)
    public Byte getDlppState() {
        return dlppState;
    }

    public void setDlppState(Byte dlppState) {
        this.dlppState = dlppState;
    }

    @Basic
    @Column(name = "dlpp_love_value", nullable = true)
    public Integer getDlppLoveValue() {
        return dlppLoveValue;
    }

    public void setDlppLoveValue(Integer dlppLoveValue) {
        this.dlppLoveValue = dlppLoveValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DLPandectPerson that = (DLPandectPerson) o;

        if (dlppId != null ? !dlppId.equals(that.dlppId) : that.dlppId != null) return false;
        if (dlppPandectId != null ? !dlppPandectId.equals(that.dlppPandectId) : that.dlppPandectId != null)
            return false;
        if (dlppState != null ? !dlppState.equals(that.dlppState) : that.dlppState != null) return false;
        if (dlppLoveValue != null ? !dlppLoveValue.equals(that.dlppLoveValue) : that.dlppLoveValue != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dlppId != null ? dlppId.hashCode() : 0;
        result = 31 * result + (dlppPandectId != null ? dlppPandectId.hashCode() : 0);
        result = 31 * result + (dlppState != null ? dlppState.hashCode() : 0);
        result = 31 * result + (dlppLoveValue != null ? dlppLoveValue.hashCode() : 0);
        return result;
    }
}
