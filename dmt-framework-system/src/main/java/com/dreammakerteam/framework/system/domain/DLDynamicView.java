package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_l_dynamic_view")
public class DLDynamicView {
    private String dldvId;
    private String dldvDynamicId;
    private String dldvUserId;
    private Timestamp dldvCreateTime;
    private Byte dldvState;

    @Id
    @Column(name = "dldv_id", nullable = false, length = 36)
    public String getDldvId() {
        return dldvId;
    }

    public void setDldvId(String dldvId) {
        this.dldvId = dldvId;
    }

    @Basic
    @Column(name = "dldv_dynamic_id", nullable = true, length = 36)
    public String getDldvDynamicId() {
        return dldvDynamicId;
    }

    public void setDldvDynamicId(String dldvDynamicId) {
        this.dldvDynamicId = dldvDynamicId;
    }

    @Basic
    @Column(name = "dldv_user_id", nullable = true, length = 36)
    public String getDldvUserId() {
        return dldvUserId;
    }

    public void setDldvUserId(String dldvUserId) {
        this.dldvUserId = dldvUserId;
    }

    @Basic
    @Column(name = "dldv_create_time", nullable = true)
    public Timestamp getDldvCreateTime() {
        return dldvCreateTime;
    }

    public void setDldvCreateTime(Timestamp dldvCreateTime) {
        this.dldvCreateTime = dldvCreateTime;
    }

    @Basic
    @Column(name = "dldv_state", nullable = true)
    public Byte getDldvState() {
        return dldvState;
    }

    public void setDldvState(Byte dldvState) {
        this.dldvState = dldvState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DLDynamicView that = (DLDynamicView) o;

        if (dldvId != null ? !dldvId.equals(that.dldvId) : that.dldvId != null) return false;
        if (dldvDynamicId != null ? !dldvDynamicId.equals(that.dldvDynamicId) : that.dldvDynamicId != null)
            return false;
        if (dldvUserId != null ? !dldvUserId.equals(that.dldvUserId) : that.dldvUserId != null) return false;
        if (dldvCreateTime != null ? !dldvCreateTime.equals(that.dldvCreateTime) : that.dldvCreateTime != null)
            return false;
        if (dldvState != null ? !dldvState.equals(that.dldvState) : that.dldvState != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dldvId != null ? dldvId.hashCode() : 0;
        result = 31 * result + (dldvDynamicId != null ? dldvDynamicId.hashCode() : 0);
        result = 31 * result + (dldvUserId != null ? dldvUserId.hashCode() : 0);
        result = 31 * result + (dldvCreateTime != null ? dldvCreateTime.hashCode() : 0);
        result = 31 * result + (dldvState != null ? dldvState.hashCode() : 0);
        return result;
    }
}
