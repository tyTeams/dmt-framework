package com.dreammakerteam.framework.system.service.impl;

import com.dreammakerteam.framework.system.domain.DSUser;
import com.dreammakerteam.framework.system.domain.DSUserRepository;
import com.dreammakerteam.framework.system.service.DSUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户服务实现
 * Created by ty850 on 2017/5/12.
 */
@Service("dSUserService")
public class DSUserServiceImpl extends DSBaseService implements DSUserService {

    @Autowired
    private DSUserRepository dDSUserRepository;

    /**
     * 通过id获取用户
     * @param id 用户id
     * @return 用户
     */
    @Override
    public DSUser get(String id) {
        return dDSUserRepository.getOne(id);
    }

    /**
     * 通过用户名及密码获取用户（密码自动加密）
     * @param username 用户名
     * @param password 用户明文密码
     * @return 用户
     */
    @Override
    public DSUser getByUsernameAndPassword(String username, String password) {
        return dDSUserRepository.findByDsuUsernameAndDsuPassword(username, password);
    }

    /**
     * 密码加密
     * @param username 用户名
     * @param password 明文密码
     * @return 密文密码
     */
    @Override
    public String enCodePassword(String username, String password) {
        return password;
    }
}
