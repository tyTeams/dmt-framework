package com.dreammakerteam.framework.system.domain;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ty850 on 2017/5/17.
 */
@Entity
@Table(name = "d_s_msg_see", schema = "dmt_demo", catalog = "")
public class DSMsgSee {
    private String dsmsId;
    private String dsmsUserId;
    private String dsmsMsgId;
    private Timestamp dsmsSeeTime;

    @Id
    @Column(name = "dsms_id")
    public String getDsmsId() {
        return dsmsId;
    }

    public void setDsmsId(String dsmsId) {
        this.dsmsId = dsmsId;
    }

    @Basic
    @Column(name = "dsms_user_id", nullable = true, length = 36)
    public String getDsmsUserId() {
        return dsmsUserId;
    }

    public void setDsmsUserId(String dsmsUserId) {
        this.dsmsUserId = dsmsUserId;
    }

    @Basic
    @Column(name = "dsms_msg_id", nullable = true, length = 36)
    public String getDsmsMsgId() {
        return dsmsMsgId;
    }

    public void setDsmsMsgId(String dsmsMsgId) {
        this.dsmsMsgId = dsmsMsgId;
    }

    @Basic
    @Column(name = "dsms_see_time", nullable = true)
    public Timestamp getDsmsSeeTime() {
        return dsmsSeeTime;
    }

    public void setDsmsSeeTime(Timestamp dsmsSeeTime) {
        this.dsmsSeeTime = dsmsSeeTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DSMsgSee dsMsgSee = (DSMsgSee) o;

        if (dsmsId != null ? !dsmsId.equals(dsMsgSee.dsmsId) : dsMsgSee.dsmsId != null) return false;
        if (dsmsUserId != null ? !dsmsUserId.equals(dsMsgSee.dsmsUserId) : dsMsgSee.dsmsUserId != null) return false;
        if (dsmsMsgId != null ? !dsmsMsgId.equals(dsMsgSee.dsmsMsgId) : dsMsgSee.dsmsMsgId != null) return false;
        if (dsmsSeeTime != null ? !dsmsSeeTime.equals(dsMsgSee.dsmsSeeTime) : dsMsgSee.dsmsSeeTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dsmsId != null ? dsmsId.hashCode() : 0;
        result = 31 * result + (dsmsUserId != null ? dsmsUserId.hashCode() : 0);
        result = 31 * result + (dsmsMsgId != null ? dsmsMsgId.hashCode() : 0);
        result = 31 * result + (dsmsSeeTime != null ? dsmsSeeTime.hashCode() : 0);
        return result;
    }
}
