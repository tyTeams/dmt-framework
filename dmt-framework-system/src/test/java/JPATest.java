import com.dreammakerteam.framework.system.Application;
import com.dreammakerteam.framework.system.domain.DSUser;
import com.dreammakerteam.framework.system.domain.DSUserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 测试
 * Created by ty850 on 2017/5/12.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class JPATest {

    @Autowired
    private DSUserRepository dDSUserRepository;

    @Test
    public void test1() {


        DSUser admin = dDSUserRepository.findByDsuUsernameAndDsuPassword("admin", "123");
        admin.setDsuName("123456");
        dDSUserRepository.save(admin);
        System.out.println(admin);

        List<DSUser> all = dDSUserRepository.findAll();
        System.out.println(all);
    }



}
