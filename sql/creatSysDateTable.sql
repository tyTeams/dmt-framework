/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2017/5/11 下午 10:27:53                        */
/*==============================================================*/


drop table if exists d_s_authority;

drop table if exists d_s_group;

drop table if exists d_s_logs;

drop table if exists d_s_msg;

drop table if exists d_s_msg_see;

drop table if exists d_s_params;

drop table if exists d_s_r_group_authority;

drop table if exists d_s_r_user_group;

drop table if exists d_s_user;

/*==============================================================*/
/* Table: d_s_authority                                         */
/*==============================================================*/
create table d_s_authority
(
   dsa_code             varchar(36) not null comment '权限编码',
   dsa_name             varchar(36) comment '权限名',
   dsa_remark           varchar(50) comment '权限说明',
   primary key (dsa_code)
);

alter table d_s_authority comment '系统-所有权限';

/*==============================================================*/
/* Table: d_s_group                                             */
/*==============================================================*/
create table d_s_group
(
   dsg_code             varchar(36) not null comment '用户组编码',
   dsg_name             varchar(36) comment '用户组名',
   dsg_type             varchar(10) comment '用户组类型',
   dsg_remark           varchar(200) comment '用户组说明',
   primary key (dsg_code)
);

alter table d_s_group comment '系统-用户组/权限组';

/*==============================================================*/
/* Table: d_s_logs                                              */
/*==============================================================*/
create table d_s_logs
(
   dsl_id               varchar(36) not null,
   dsl_type             varchar(10) comment '日志主要类型',
   dsl_type2            varchar(10) comment '日志次要类型',
   dsl_creat_time       datetime comment '操作时间',
   dsl_user_id          varchar(36) comment '操作来源用户id',
   dsl_target_id        varchar(36) comment '操作主要目标id',
   dsl_target_type      varchar(10) comment '操作主要目标类型',
   dsl_target_id2       varchar(36) comment '操作次要目标id',
   dsl_target_type2     varchar(10) comment '操作次要目标类型',
   dsl_remark           varchar(200) comment '其它信息',
   primary key (dsl_id)
);

alter table d_s_logs comment '系统-操作日志';

/*==============================================================*/
/* Table: d_s_msg                                               */
/*==============================================================*/
create table d_s_msg
(
   dsm_id               varchar(36) not null,
   dsm_target_id        varchar(36) comment '被通知用户/用户组',
   dsm_type             varchar(10) comment '通知类型',
   dsm_title            varchar(50) comment '标题',
   dsm_content          varchar(200) comment '内容',
   dsm_state            varchar(10) comment '状态',
   dsm_creat_time       datetime comment '发送日期',
   dsm_see_time         datetime comment '查看日期',
   primary key (dsm_id)
);

alter table d_s_msg comment '系统-通知';

/*==============================================================*/
/* Table: d_s_msg_see                                           */
/*==============================================================*/
create table d_s_msg_see
(
   dsms_id              varchar(36) not null,
   dsms_user_id         varchar(36) comment '用户id',
   dsms_msg_id          varchar(36) comment '通知id',
   dsms_see_time        datetime comment '查看时间',
   primary key (dsms_id)
);

alter table d_s_msg_see comment '系统-用户通知查看';

/*==============================================================*/
/* Table: d_s_params                                            */
/*==============================================================*/
create table d_s_params
(
   dsp_key              varchar(36) not null comment '键',
   dsp_value            varchar(36) comment '值',
   dsp_parent           varchar(36) comment '父',
   primary key (dsp_key)
);

alter table d_s_params comment '系统-系统配置';

/*==============================================================*/
/* Table: d_s_r_group_authority                                 */
/*==============================================================*/
create table d_s_r_group_authority
(
   dsrga_id             varchar(36) not null,
   dsrga_group_code     varchar(36) comment '用户组编码',
   dsrga_authority_code varchar(36) comment '权限编码',
   dsrga_remark         varchar(100) comment '其它信息',
   primary key (dsrga_id)
);

alter table d_s_r_group_authority comment '系统-用户组权限';

/*==============================================================*/
/* Table: d_s_r_user_group                                      */
/*==============================================================*/
create table d_s_r_user_group
(
   dsrug_id             varchar(36) not null,
   dsrug_user_id        varchar(36) comment '用户id',
   dsrug_code           varchar(36) comment '权限组编码',
   primary key (dsrug_id)
);

alter table d_s_r_user_group comment '系统-用户附加权限组关联';

/*==============================================================*/
/* Table: d_s_user                                              */
/*==============================================================*/
create table d_s_user
(
   dsu_id               varchar(36) not null,
   dsu_username         varchar(36) comment '用户登陆名',
   dsu_name             varchar(36) comment '用户显示名',
   dsu_password         varchar(36) comment '密码',
   dsu_last_login_time  datetime comment '上次登陆时间',
   dsu_group_code       varchar(36) comment '主用户组编码',
   dsu_creat_time       datetime comment '创建时间',
   dsu_change_time      datetime comment '修改时间',
   dsu_photo            varchar(50) comment '头像路径',
   dsu_remark           varchar(200) comment '签名',
   primary key (dsu_id)
);

alter table d_s_user comment '系统-用户';

